mongo-10gen:
  file:
    - managed
    - name: /etc/yum.repos.d/10gen.repo
    - source: salt://mongo/10gen.repo
    - skip_verify: True
  pkg:
    - name: mongo-10gen-server
    - installed
  service:
    - running
    - enable: True
    - name: mongod
    - require:
      - pkg: mongo-10gen-server

