nginx:
  file:
    - managed
    - name: /etc/yum.repos.d/nginx.repo
    - source: salt://nginx/nginx.repo
    - skip_verify: True
  pkg:
    - installed
    - refresh: True
  service:
    - running
    - enable: True
    - name: nginx
    - require:
      - pkg: nginx

