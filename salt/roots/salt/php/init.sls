phpcorepkgs:
  pkg.installed:
    - pkgs:
      - php
      - php-bcmath
      - php-common
      - php-fpm
      - php-gd
      - php-imap
      - php-intl
      - php-mbstring
      - php-mysql
      - php-pdo
      - php-xml
      - php-xmlrpc
      - php-pecl-memcache

/etc/php.ini:
  file:
    - managed
    - source: salt://php/php.ini
    - user: root
    - group: root
    - mode: 644

/etc/php-fpm.d/www.conf:
  file:
    - managed
    - source: salt://php/www.conf
    - user: root
    - group: root
    - mode: 644

/etc/nginx/conf.d/default.conf:
  file:
    - managed
    - source: salt://php/default.conf
    - user: root
    - group: root
    - mode: 644

/usr/share/nginx/html/index.php:
  file:
    - managed
    - source: salt://php/index.php
    - user: nginx
    - group: nginx
    - mode: 664
    - require:
      - pkg: nginx

php-fpm:
  pkg:
    - installed
    - refresh: True
  service:
    - running
    - enable: True
    - name: php-fpm
    - require:
      - pkg: php-fpm
      - pkg: nginx
      - file: /etc/php-fpm.d/www.conf
      - file: /etc/php.ini
      - file: /etc/nginx/conf.d/default.conf
      - file: /usr/share/nginx/html/index.php
