ntpd:
  pkg:
    - name: ntp
    - installed
  service:
    - running
    - enable: True
    - name: ntpd
    - require:
      - pkg: ntp

