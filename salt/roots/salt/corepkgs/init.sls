corepkgs:
  pkg.installed:
    - pkgs:
      - mc
      - htop
      - ntsysv
      - nc
      - system-config-network-tui
      - nano
